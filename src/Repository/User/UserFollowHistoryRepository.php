<?php

declare(strict_types=1);

namespace App\Repository\User;

use App\Entity\User\UserFollowHistory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserFollowHistory|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserFollowHistory|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserFollowHistory[]    findAll()
 * @method UserFollowHistory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserFollowHistoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserFollowHistory::class);
    }
}
