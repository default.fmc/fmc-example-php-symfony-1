<?php

declare(strict_types=1);

namespace App\Repository\Post;

use App\Entity\Post\PostUserSmile;
use App\Entity\User\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PostUserSmile|null find($id, $lockMode = null, $lockVersion = null)
 * @method PostUserSmile|null findOneBy(array $criteria, array $orderBy = null)
 * @method PostUserSmile[]    findAll()
 * @method PostUserSmile[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostUserSmileRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PostUserSmile::class);
    }

    /**
     * @param User $user
     * @return PostUserSmile[]
     */
    public function smilePosts(User $user): array
    {
        return $this->createQueryBuilder('postUserSmile')
            ->join('postUserSmile.post', 'post')
            ->join('post.user', 'user')
            ->where('user = :user')
            ->setParameter('user', $user)
            ->orderBy('postUserSmile.createdAt', 'DESC')
            ->setMaxResults(100)
            ->getQuery()
            ->getResult()
        ;
    }
}
