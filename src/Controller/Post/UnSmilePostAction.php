<?php

declare(strict_types=1);

namespace App\Controller\Post;

use App\Entity\Post\Post;
use App\Entity\Post\PostUserSmile;
use App\Entity\User\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;

class UnSmilePostAction
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * @var User
     */
    private $user;

    /**
     * @param EntityManagerInterface $manager
     * @param Security $security
     */
    public function __construct(EntityManagerInterface $manager, Security $security)
    {
        $this->manager = $manager;
        $this->user = $security->getUser();
    }

    /**
     * @param Post $data
     * @return Post
     */
    public function __invoke(Post $data): Post
    {
        if ($postUserSmile = $this->manager->getRepository(PostUserSmile::class)->findOneBy(
            ['post' => $data, 'user' => $this->user]
        )) {
            $data->removePostUserSmile($postUserSmile);

            $this->manager->persist($data);
            $this->manager->flush();
        }

        return $data;
    }
}
