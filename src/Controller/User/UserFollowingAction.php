<?php

declare(strict_types=1);

namespace App\Controller\User;

use App\Entity\User\User;
use App\Entity\User\UserFollowHistory;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;

class UserFollowingAction
{
    /**
     * @var Security
     */
    private $security;

    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * @param Security $security
     * @param EntityManagerInterface $manager
     */
    public function __construct(Security $security, EntityManagerInterface $manager)
    {
        $this->security = $security;
        $this->manager = $manager;
    }

    /**
     * @param User $data
     * @return User
     */
    public function __invoke(User $data): User
    {
        /** @var User $user */
        $user = $this->security->getUser();

        if (!$user->getFollowing()->contains($data)) {
            $user->addFollowing($data);

            $history = (new UserFollowHistory())
                ->setFollowing($user)
                ->setFollower($data)
            ;

            $this->manager->persist($history);
            $this->manager->persist($user);
            $this->manager->flush();
        }

        return $data;
    }
}
