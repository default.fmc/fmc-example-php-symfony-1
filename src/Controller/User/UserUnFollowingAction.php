<?php

declare(strict_types=1);

namespace App\Controller\User;

use App\Entity\User\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;

class UserUnFollowingAction
{
    /**
     * @var Security
     */
    private $security;

    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * @param Security               $security
     * @param EntityManagerInterface $manager
     */
    public function __construct(Security $security, EntityManagerInterface $manager)
    {
        $this->security = $security;
        $this->manager = $manager;
    }

    public function __invoke(User $data): User
    {
        /** @var User $user */
        $user = $this->security->getUser();

        $user->removeFollowing($data);

        $this->manager->persist($user);
        $this->manager->flush();

        return $data;
    }
}
