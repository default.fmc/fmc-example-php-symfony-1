<?php

declare(strict_types=1);

namespace App\Controller\User;

use App\Entity\User\User;
use App\Repository\User\UserRepository;
use App\Service\User\NotificationHandler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class NotificationsAction extends AbstractController
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var NotificationHandler
     */
    private $handler;

    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * @param SerializerInterface $serializer
     * @param NotificationHandler $handler
     * @param UserRepository $repository
     */
    public function __construct(
        SerializerInterface $serializer,
        NotificationHandler $handler,
        UserRepository $repository
    ) {
        $this->serializer = $serializer;
        $this->handler = $handler;
        $this->repository = $repository;
    }

    /**
     * @Route(
     *     name="user_notifications",
     *     path="/api/users/{id}/notifications",
     *     methods={"GET"}
     * )
     * @param int $id
     * @return JsonResponse
     */
    public function __invoke(int $id)
    {
        if (!$user = $this->repository->find($id)) {
            throw new NotFoundHttpException('User not found.');
        }

        $result = $this->handler->handle($user);

        $jsonld = $this->serializer->serialize($result, 'jsonld', ['groups' => ['user:notifications']]);

        return new JsonResponse($jsonld, Response::HTTP_OK, [], true);
    }
}
