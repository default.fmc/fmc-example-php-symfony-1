<?php

declare(strict_types=1);

namespace App\Controller\User;

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Entity\User\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserChangePasswordAction
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * @param ValidatorInterface           $validator
     * @param UserPasswordEncoderInterface $encoder
     * @param EntityManagerInterface       $manager
     */
    public function __construct(ValidatorInterface $validator, UserPasswordEncoderInterface $encoder, EntityManagerInterface $manager)
    {
        $this->validator = $validator;
        $this->encoder = $encoder;
        $this->manager = $manager;
    }

    public function __invoke(User $data): User
    {
        $this->validator->validate($data, ['groups' => ['change-password:put']]);

        $encodedNewPassword = $this->encoder->encodePassword($data, $data->getNewPassword());
        $data->setPassword($encodedNewPassword);

        $this->manager->persist($data);
        $this->manager->flush();

        return $data;
    }
}
