<?php

declare(strict_types=1);

namespace App\Controller\User;

use App\Entity\User\User;

class UserUpdatePointsAction
{
    public function __invoke(User $data)
    {
        return $data;
    }
}
