<?php

declare(strict_types=1);

namespace App\Controller;

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Entity\User\User;
use App\Service\Mailer\ResetPasswordMail;
use App\Service\User\Authentication\LoginCredentials;
use App\Service\User\Authentication\UserAvatarHandler;
use App\Service\User\Authentication\UserResetPassword;
use App\Service\User\UserAccessToken;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/api")
 */
class SecurityController extends AbstractController
{
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedServices(): array
    {
        return array_merge(
            parent::getSubscribedServices(),
            [
                UserAccessToken::class => UserAccessToken::class,
                UserPasswordEncoderInterface::class => UserPasswordEncoderInterface::class,
                LoginCredentials::class => LoginCredentials::class,
                SerializerInterface::class => SerializerInterface::class,
                ValidatorInterface::class => ValidatorInterface::class,
                UserAvatarHandler::class => UserAvatarHandler::class,
            ]
        );
    }

    /**
     * @Route("/users/login", name="app_users_login", methods={"POST"})
     */
    public function userLogin(): JsonResponse
    {
        return $this->login('ROLE_USER', ['groups' => ['user:login']]);
    }

    /**
     * @Route("/admin/login", name="app_admin_login", methods={"POST"})
     */
    public function adminLogin(): JsonResponse
    {
        return $this->login('ROLE_ADMIN', ['groups' => ['admin:login']]);
    }

    /**
     * @Route("/logout", name="app_logout", methods={"GET"})
     * @return Response
     */
    public function logout(): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        $user->setAccessToken(null);
        $this->getDoctrine()->getManager()->persist($user);
        $this->getDoctrine()->getManager()->flush();

        return new Response(null);
    }

    /**
     * @Route("/reset_password", name="app_reset_password", methods={"POST"})
     * @param UserResetPassword $userResetPassword
     * @param ResetPasswordMail $mail
     * @return Response
     */
    public function resetPassword(UserResetPassword $userResetPassword, ResetPasswordMail $mail): Response
    {
        $this->get(ValidatorInterface::class)->validate($userResetPassword, ['groups' => ['user:reset-password']]);

        $repository = $this->getDoctrine()->getRepository(User::class);

        if (!$user = $repository->findOneBy(['email' => $userResetPassword->getEmail()])) {
            throw new HttpException(
                Response::HTTP_NOT_FOUND,
                sprintf('User witch email "%s" not found!', $userResetPassword->getEmail())
            );
        }

        $password = $userResetPassword->reset($user);

        $mail->send($user->getEmail(), $password);

        return new Response();
    }

    /**
     * @param string $role
     * @param array  $context
     * @return JsonResponse
     */
    private function login(string $role = 'ROLE_USER', array $context = []): JsonResponse
    {
        /** @var LoginCredentials $loginCredentials */
        $loginCredentials = $this->get(LoginCredentials::class);
        $this->get(ValidatorInterface::class)->validate($loginCredentials, ['groups' => ['user:post']]);

        $repository = $this->getDoctrine()->getRepository(User::class);

        $encoder = $this->get(UserPasswordEncoderInterface::class);

        if (($user = $repository->findOneBy(['email' => $loginCredentials->getEmail()])) &&
            $encoder->isPasswordValid($user, $loginCredentials->getPassword()) &&
            \in_array($role, $user->getRoles(), true)) {
            $this->get(UserAccessToken::class)->update($user);

            $this->get(UserAvatarHandler::class)->handle($user);

            return new JsonResponse(
                $this->get(SerializerInterface::class)->serialize($user, 'jsonld', $context),
                Response::HTTP_OK,
                [],
                true
            );
        }

        throw new HttpException(Response::HTTP_UNAUTHORIZED, 'Invalid credentials');
    }
}
