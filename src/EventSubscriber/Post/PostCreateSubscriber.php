<?php

declare(strict_types=1);

namespace App\EventSubscriber\Post;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Post\Post;
use App\Entity\Project\Project;
use App\Entity\User\User;
use App\Exception\PaymentRequiredException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Security;

class PostCreateSubscriber implements EventSubscriberInterface
{
    /**
     * @var Security
     */
    private $security;

    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * @param Security               $security
     * @param EntityManagerInterface $manager
     */
    public function __construct(Security $security, EntityManagerInterface $manager)
    {
        $this->security = $security;
        $this->manager = $manager;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => ['preValidate', EventPriorities::PRE_VALIDATE],
        ];
    }

    /**
     * @param ViewEvent $event
     * @throws PaymentRequiredException
     */
    public function preValidate(ViewEvent $event): void
    {
        $post = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if (!$post instanceof Post || Request::METHOD_POST !== $method || $post->getId() !== null) {
            return;
        }

        /** @var User $user */
        $user = $this->security->getUser();

        if ($post->getDonationSum() > $user->getPoints()) {
            throw new PaymentRequiredException('Not enough points to donate.');
        }

        $post->setUser($user);

        $user->setPoints($user->getPoints() - $post->getDonationSum());

        /** @var Project $project */
        $project = $post->getProject();

        $project->setCollected($project->getCollected() + $post->getDonationSum());

        $this->manager->persist($user);
        $this->manager->persist($project);
    }
}
