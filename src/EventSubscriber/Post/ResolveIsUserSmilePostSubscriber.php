<?php

declare(strict_types=1);

namespace App\EventSubscriber\Post;

use ApiPlatform\Core\EventListener\EventPriorities;
use ApiPlatform\Core\Util\RequestAttributesExtractor;
use App\Entity\Post\Post;
use App\Entity\User\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Security;

final class ResolveIsUserSmilePostSubscriber implements EventSubscriberInterface
{
    /**
     * @var User
     */
    private $user;

    /**
     * @param Security $security
     */
    public function __construct(Security $security)
    {
        $this->user = $security->getUser();
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => ['onPreSerialize', EventPriorities::PRE_SERIALIZE],
        ];
    }

    public function onPreSerialize(ViewEvent $event): void
    {
        $controllerResult = $event->getControllerResult();
        $request = $event->getRequest();

        if ($controllerResult instanceof Response || !$request->attributes->getBoolean('_api_respond', true)) {
            return;
        }

        if (!($attributes = RequestAttributesExtractor::extractAttributes($request)) ||
            !is_a($attributes['resource_class'], Post::class, true)) {
            return;
        }

        $posts = $controllerResult;

        if (!is_iterable($posts)) {
            $posts = [$posts];
        }

        foreach ($posts as $post) {
            if (!$post instanceof Post) {
                continue;
            }

            $isSmile = $post->getPostUserSmiles()->exists(function ($key, $el) {
                return $el->getUser() === $this->user;
            });

            $post->setIsSmile($isSmile);
        }
    }
}
