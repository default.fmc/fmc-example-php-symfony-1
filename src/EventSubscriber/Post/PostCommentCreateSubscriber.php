<?php

declare(strict_types=1);

namespace App\EventSubscriber\Post;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Post\PostComment;
use App\Entity\User\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Security;

class PostCommentCreateSubscriber implements EventSubscriberInterface
{
    /**
     * @var Security
     */
    private $security;

    /**
     * @param Security $security
     */
    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => ['preValidate', EventPriorities::PRE_VALIDATE],
        ];
    }

    /**
     * @param ViewEvent $event
     */
    public function preValidate(ViewEvent $event): void
    {
        $postComment = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if (!$postComment instanceof PostComment || Request::METHOD_POST !== $method || $postComment->getId() !== null) {
            return;
        }

        /** @var User $user */
        $user = $this->security->getUser();

        $postComment->setUser($user);
    }
}
