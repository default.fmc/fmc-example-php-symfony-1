<?php

declare(strict_types=1);

namespace App\EventSubscriber\Post;

use ApiPlatform\Core\EventListener\EventPriorities;
use ApiPlatform\Core\Util\RequestAttributesExtractor;
use App\Entity\MediaObject;
use App\Entity\Post\Post;
use App\Entity\User\User;
use Liip\ImagineBundle\Service\FilterService;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;

final class ResolvePostMediaUrlSubscriber implements EventSubscriberInterface
{
    /**
     * @var FilterService
     */
    private $filter;

    /**
     * @var ParameterBagInterface
     */
    private $bag;

    /**
     * @param FilterService $filter
     * @param ParameterBagInterface $bag
     */
    public function __construct(FilterService $filter, ParameterBagInterface $bag)
    {
        $this->filter = $filter;
        $this->bag = $bag;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => ['onPreSerialize', EventPriorities::PRE_SERIALIZE],
        ];
    }

    public function onPreSerialize(ViewEvent $event): void
    {
        $controllerResult = $event->getControllerResult();
        $request = $event->getRequest();

        if ($controllerResult instanceof Response || !$request->attributes->getBoolean('_api_respond', true)) {
            return;
        }

        if (!($attributes = RequestAttributesExtractor::extractAttributes($request)) ||
            !is_a($attributes['resource_class'], Post::class, true)) {
            return;
        }

        $posts = $controllerResult;

        if (!is_iterable($posts)) {
            $posts = [$posts];
        }

        foreach ($posts as $post) {
            if (!$post instanceof Post) {
                continue;
            }

            $post->setCountComments($post->getComments()->count());

            foreach ($post->getImages() as $image) {
                $resourcePath = $this->filter->getUrlOfFilteredImage(
                    $this->bag->get('images_prefix') . '/' . $image->getName(),
                    'post_thumbnail'
                );

                $image->setUrl($resourcePath);
            }

            /** @var User $user */
            $user = $post->getUser();

            if ($user->getAvatar() !== null) {
                $this->avatarResource($user->getAvatar());
            }

            foreach ($post->getPostUserSmiles() as $postUserSmile) {
                $userSmile = $postUserSmile->getUser();
                if ($userSmile->getAvatar() !== null) {
                    $this->avatarResource($userSmile->getAvatar());
                }
            }
        }
    }

    /**
     * @param MediaObject $avatar
     */
    private function avatarResource(MediaObject $avatar): void
    {
        $resourcePath = $this->filter->getUrlOfFilteredImage(
            $this->bag->get('images_prefix') . '/' . $avatar->getName(),
            'user_avatar_thumbnail'
        );

        $avatar->setUrl($resourcePath);
    }
}
