<?php

declare(strict_types=1);

namespace App\EventSubscriber\Project;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Project\Project;
use App\Entity\User\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Security;

class ProjectPostSubscriber implements EventSubscriberInterface
{
    /**
     * @var Security
     */
    private $security;

    /**
     * @param Security $security
     */
    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => ['preValidate', EventPriorities::PRE_VALIDATE],
        ];
    }

    /**
     * @param ViewEvent $event
     */
    public function preValidate(ViewEvent $event): void
    {
        $project = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if (!$project instanceof Project || Request::METHOD_POST !== $method || $project->getId() !== null) {
            return;
        }

        /** @var User $user */
        $user = $this->security->getUser();

        $project->setUser($user);
    }
}
