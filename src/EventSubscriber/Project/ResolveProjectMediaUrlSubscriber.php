<?php

declare(strict_types=1);

namespace App\EventSubscriber\Project;

use ApiPlatform\Core\EventListener\EventPriorities;
use ApiPlatform\Core\Util\RequestAttributesExtractor;
use App\Entity\Project\Project;
use Liip\ImagineBundle\Service\FilterService;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;

final class ResolveProjectMediaUrlSubscriber implements EventSubscriberInterface
{
    /**
     * @var FilterService
     */
    private $filter;

    /**
     * @var ParameterBagInterface
     */
    private $bag;

    /**
     * @param FilterService $filter
     * @param ParameterBagInterface $bag
     */
    public function __construct(FilterService $filter, ParameterBagInterface $bag)
    {
        $this->filter = $filter;
        $this->bag = $bag;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => ['onPreSerialize', EventPriorities::PRE_SERIALIZE],
        ];
    }

    public function onPreSerialize(ViewEvent $event): void
    {
        $controllerResult = $event->getControllerResult();
        $request = $event->getRequest();

        if ($controllerResult instanceof Response || !$request->attributes->getBoolean('_api_respond', true)) {
            return;
        }

        if (!($attributes = RequestAttributesExtractor::extractAttributes($request)) ||
            !is_a($attributes['resource_class'], Project::class, true)) {
            return;
        }

        $projects = $controllerResult;

        if (!is_iterable($projects)) {
            $projects = [$projects];
        }

        foreach ($projects as $project) {
            if (!$project instanceof Project) {
                continue;
            }

            $resourcePath = $this->filter->getUrlOfFilteredImage(
                $this->bag->get('images_prefix') . '/' . $project->getImage()->getName(),
                'project_thumbnail'
            );

            $project->getImage()->setUrl($resourcePath);

            foreach ($project->getPosts() as $post) {
                if ($avatar = $post->getUser()->getAvatar()) {
                    $resourcePath = $this->filter->getUrlOfFilteredImage(
                        $this->bag->get('images_prefix') . '/' . $avatar->getName(),
                        'user_avatar_thumbnail'
                    );

                    $avatar->setUrl($resourcePath);
                }
            }
        }
    }
}
