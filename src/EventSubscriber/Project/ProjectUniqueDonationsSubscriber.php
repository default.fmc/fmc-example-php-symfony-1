<?php

declare(strict_types=1);

namespace App\EventSubscriber\Project;

use ApiPlatform\Core\EventListener\EventPriorities;
use ApiPlatform\Core\Util\RequestAttributesExtractor;
use App\Entity\Project\Project;
use App\Repository\User\UserRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;

final class ProjectUniqueDonationsSubscriber implements EventSubscriberInterface
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => ['onPreSerialize', EventPriorities::PRE_SERIALIZE],
        ];
    }

    public function onPreSerialize(ViewEvent $event): void
    {
        $controllerResult = $event->getControllerResult();
        $request = $event->getRequest();

        if ($controllerResult instanceof Response || !$request->attributes->getBoolean('_api_respond', true)) {
            return;
        }

        if (!($attributes = RequestAttributesExtractor::extractAttributes($request)) ||
            !is_a($attributes['resource_class'], Project::class, true)) {
            return;
        }

        $projects = $controllerResult;

        if (!is_iterable($projects)) {
            $projects = [$projects];
        }

        foreach ($projects as $project) {
            if (!$project instanceof Project) {
                continue;
            }

            $project->setUniqueDonationsCount($this->repository->uniqueDonationsCount($project));
        }
    }
}
