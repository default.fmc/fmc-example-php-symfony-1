<?php

declare(strict_types=1);

namespace App\EventSubscriber\User;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\User\User;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class UpdatePointsSubscriber implements EventSubscriberInterface
{
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => ['postValidate', EventPriorities::POST_VALIDATE],
        ];
    }

    /**
     * @param ViewEvent $event
     */
    public function postValidate(ViewEvent $event): void
    {
        $user = $event->getControllerResult();
        $request = $event->getRequest();

        if (!$user instanceof User || Request::METHOD_POST !== $request->getMethod() || stripos($request->getRequestUri(), 'points') === false) {
            return;
        }

        $user->setPoints($user->getPoints() + $user->getPointsUp());
    }
}
