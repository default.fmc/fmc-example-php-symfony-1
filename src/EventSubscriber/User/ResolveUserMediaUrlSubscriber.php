<?php

declare(strict_types=1);

namespace App\EventSubscriber\User;

use ApiPlatform\Core\EventListener\EventPriorities;
use ApiPlatform\Core\Util\RequestAttributesExtractor;
use App\Entity\User\User;
use Liip\ImagineBundle\Service\FilterService;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;

final class ResolveUserMediaUrlSubscriber implements EventSubscriberInterface
{
    /**
     * @var FilterService
     */
    private $filter;

    /**
     * @var ParameterBagInterface
     */
    private $bag;

    /**
     * @param FilterService         $filter
     * @param ParameterBagInterface $bag
     */
    public function __construct(FilterService $filter, ParameterBagInterface $bag)
    {
        $this->filter = $filter;
        $this->bag = $bag;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => ['onPreSerialize', EventPriorities::PRE_SERIALIZE],
        ];
    }

    public function onPreSerialize(ViewEvent $event): void
    {
        $controllerResult = $event->getControllerResult();
        $request = $event->getRequest();

        if ($controllerResult instanceof Response || !$request->attributes->getBoolean('_api_respond', true)) {
            return;
        }

        if (!($attributes = RequestAttributesExtractor::extractAttributes($request)) ||
            !is_a($attributes['resource_class'], User::class, true)) {
            return;
        }

        $users = $controllerResult;

        if (!is_iterable($users)) {
            $users = [$users];
        }

        foreach ($users as $user) {
            if (!$user instanceof User) {
                continue;
            }

            if ($user->getAvatar()) {
                $resourcePath = $this->filter->getUrlOfFilteredImage($this->bag->get('images_prefix') . '/' . $user->getAvatar()->getName(), 'user_avatar_thumbnail');

                $user->getAvatar()->setUrl($resourcePath);
            }
        }
    }
}
