<?php

declare(strict_types=1);

namespace App\EventSubscriber\PostUserSmile;

use ApiPlatform\Core\EventListener\EventPriorities;
use ApiPlatform\Core\Util\RequestAttributesExtractor;
use App\Entity\Post\PostUserSmile;
use Liip\ImagineBundle\Service\FilterService;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;

final class ResolveIsUserAvatarSubscriber implements EventSubscriberInterface
{
    /**
     * @var FilterService
     */
    private $filter;

    /**
     * @var ParameterBagInterface
     */
    private $bag;

    /**
     * @param FilterService $filter
     * @param ParameterBagInterface $bag
     */
    public function __construct(FilterService $filter, ParameterBagInterface $bag)
    {
        $this->filter = $filter;
        $this->bag = $bag;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => ['onPreSerialize', EventPriorities::PRE_SERIALIZE],
        ];
    }

    public function onPreSerialize(ViewEvent $event): void
    {
        $controllerResult = $event->getControllerResult();
        $request = $event->getRequest();

        if ($controllerResult instanceof Response || !$request->attributes->getBoolean('_api_respond', true)) {
            return;
        }

        if (!($attributes = RequestAttributesExtractor::extractAttributes($request)) ||
            !is_a($attributes['resource_class'], PostUserSmile::class, true)) {
            return;
        }

        $postUserSmiles = $controllerResult;

        if (!is_iterable($postUserSmiles)) {
            $postUserSmiles = [$postUserSmiles];
        }

        foreach ($postUserSmiles as $postUserSmile) {
            if (!$postUserSmile instanceof PostUserSmile) {
                continue;
            }

            $avatar = $postUserSmile->getUser()->getAvatar();

            if ($avatar !== null) {
                $resourcePath = $this->filter->getUrlOfFilteredImage(
                    $this->bag->get('images_prefix') . '/' . $avatar->getName(),
                    'user_avatar_thumbnail'
                );

                $avatar->setUrl($resourcePath);
            }
        }
    }
}
