<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\User\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class FollowerFixtures extends Fixture implements OrderedFixtureInterface
{
    use FixturesOrder;

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager): void
    {
        /** @var User $test1 */
        $test1 = $this->getReference(UserFixtures::class . '_1');

        for ($i = 2; $i < UserFixtures::COUNT; $i++) {
            /** @var User $user */
            $user = $this->getReference(UserFixtures::class . '_' . $i);

            $test1->addFollowing($user);
        }

        // $manager->persist($test1);
       // $manager->flush();
    }
}
