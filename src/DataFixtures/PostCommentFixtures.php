<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Post\Post;
use App\Entity\Post\PostComment;
use App\Entity\User\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class PostCommentFixtures extends Fixture implements OrderedFixtureInterface
{
    use FixturesOrder;

    public const COUNT = PostFixtures::COUNT * 2;

    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function load(ObjectManager $manager): void
    {
        for ($i = 0; $i < self::COUNT; $i++) {
            /** @var User $user */
            $user = $this->getReference(UserFixtures::class . '_' . random_int(1, UserFixtures::COUNT - 1));

            /** @var Post $post */
            $post = $this->getReference(PostFixtures::class . '_' . random_int(0, PostFixtures::COUNT - 1));

            $postComment = (new PostComment())
                ->setText('comment ' . ($i + 1))
                ->setUser($user)
                ->setPost($post)
            ;

            $manager->persist($postComment);
        }

        $manager->flush();
    }
}
