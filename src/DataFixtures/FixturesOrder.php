<?php

declare(strict_types=1);

namespace App\DataFixtures;

trait FixturesOrder
{
    /**
     * @var int[]
     */
    private $orderNumber = [
        MediaFixtures::class,
        UserFixtures::class,
        CategoryFixtures::class,
        ProjectFixtures::class,
        FollowerFixtures::class,
        PostFixtures::class,
        PostCommentFixtures::class,
    ];

    /**
     * @param string $fixtureClass
     *
     * @return int
     */
    public function getOrderNumber(string $fixtureClass): int
    {
        return array_search($fixtureClass, $this->orderNumber, true) + 1;
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder(): int
    {
        return $this->getOrderNumber(self::class);
    }
}
