<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\MediaObject;
use App\Entity\Post\Post;
use App\Entity\Project\Project;
use App\Entity\User\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class PostFixtures extends Fixture implements OrderedFixtureInterface
{
    use FixturesOrder;

    public const COUNT = ProjectFixtures::COUNT * 2;
    public const MAX_IMAGES = 5;

    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function load(ObjectManager $manager): void
    {
        for ($i = 0; $i < self::COUNT; $i++) {
            /** @var User $user */
            $user = $this->getReference(UserFixtures::class . '_' . random_int(1, UserFixtures::COUNT - 1));

            /** @var Project $project */
            $project = $this->getReference(ProjectFixtures::class . '_' . random_int(0, ProjectFixtures::COUNT - 1));

            $donationSum = random_int(10, (int) $project->getCollectionAmount());

            $post = (new Post())
                ->setCaption('Donation post ' . ($i + 1))
                ->setDonationSum($donationSum)
                ->setUser($user)
                ->setProject($project)
            ;

            $project->setCollected($project->getCollected() + $donationSum);

            for ($j = 0; $j < random_int(1, self::MAX_IMAGES); $j++) {
                $image = (new MediaObject())
                    ->setName(MediaFixtures::POST_IMAGE)
                ;

                $manager->persist($image);

                $post->addImage($image);
            }

            $manager->persist($project);
            $manager->persist($post);

            $this->addReference(self::class . '_' . $i, $post);
        }

        $manager->flush();
    }
}
