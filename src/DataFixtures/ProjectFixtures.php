<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Category\Category;
use App\Entity\MediaObject;
use App\Entity\Project\Project;
use App\Entity\User\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ProjectFixtures extends Fixture implements OrderedFixtureInterface
{
    use FixturesOrder;

    public const COUNT = 10;

    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    public function load(ObjectManager $manager): void
    {
        for ($i = 0; $i < self::COUNT; $i++) {
            $project = new Project();

            $project
                ->setTitle('Project ' . ($i + 1))
                ->setPlace('Tanzania, Mozambique')
                ->setCollectionAmount(random_int(100, 5000))
                ->setStartAt($this->dateTime('-' . random_int(1, 5)))
                ->setEndAt($this->dateTime('+' . random_int(1, 30)))
                ->setDescription(
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
                )
            ;

            /** @var User $user */
            $user = $this->getReference(UserFixtures::class . '_0');
            $project->setUser($user);

            /** @var Category $category */
            $category = $this->getReference(CategoryFixtures::class . '_' . random_int(0, \count(CategoryFixtures::CATEGORIES) - 1));
            $project->setCategory($category);

            /** @var MediaObject $image */
            $image = $this->getReference('media_' . self::class . '_' . $i);
            $project->setImage($image);

            $manager->persist($project);

            $this->addReference(self::class . '_' . $i, $project);
        }

        $manager->flush();
    }

    /**
     * @param string $countDays
     * @throws \Exception
     * @return \DateTime
     */
    private function dateTime(string $countDays): \DateTime
    {
        return (new \DateTime('now ' . $countDays . ' day'))->setTimezone(new \DateTimeZone('GMT-0'));
    }
}
