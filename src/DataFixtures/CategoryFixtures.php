<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Category\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class CategoryFixtures extends Fixture implements OrderedFixtureInterface
{
    use FixturesOrder;

    public const CATEGORIES = [
        [
            'name' => 'Healthcare',
            'description' => null,
        ],
        [
            'name' => 'Infrastructure',
            'description' => null,
        ],
        [
            'name' => 'Education',
            'description' => null,
        ],
    ];

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager): void
    {
        foreach (self::CATEGORIES as $i => $item) {
            $category = new Category();

            $category
                ->setName($item['name'])
                ->setDescription($item['description'])
            ;

            $manager->persist($category);

            $this->addReference(self::class . '_' . $i, $category);
        }

        $manager->flush();
    }
}
