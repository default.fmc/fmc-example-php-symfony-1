<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\MediaObject;
use App\Entity\User\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

class UserFixtures extends Fixture implements OrderedFixtureInterface
{
    use FixturesOrder;

    public const COUNT = 10;

    public const PASSWORD = 'qweqweqwe';

    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * @var TokenGeneratorInterface
     */
    private $tokenGenerator;

    /**
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param TokenGeneratorInterface      $tokenGenerator
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder, TokenGeneratorInterface $tokenGenerator)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->tokenGenerator = $tokenGenerator;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager): void
    {
        for ($i = 0; $i < self::COUNT; $i++) {
            $user = new User();

            if ($i < 2) {
                if ($i === 0) {
                    $user
                        ->setEmail('admin@test.com')
                        ->setRoles(['ROLE_ADMIN'])
                        ->setNickname('admin')
                        ->setFullName('Admin Admin')
                        ->setAccessToken('vc0Vcfj9qYICMX0IemNlupDhMyn4rayCB7IwDx7OP2Y')
                    ;
                } else {
                    $user
                        ->setEmail('admin2@test.com')
                        ->setRoles(['ROLE_ADMIN'])
                        ->setNickname('admin2')
                        ->setFullName('Admin2 Admin2')
                        ->setAccessToken('vc0Vcfj9qYICMX0IemNlupDhMyn4rayCB7IwDx7OP2E')
                    ;
                }
            } else {
                $user
                    ->setEmail('test' . ($i - 1) . '@test.com')
                    ->setNickname('test' . ($i - 1))
                    ->setFullName('Test Test')
                    ->setAccessToken($this->tokenGenerator->generateToken())
                ;

                /** @var MediaObject $avatar */
                $avatar = $this->getReference('media_' . self::class . '_' . $i);

                $user->setAvatar($avatar);
            }

            $user->setPassword(
                $this->passwordEncoder->encodePassword(
                    $user,
                    self::PASSWORD
                )
            );

            $manager->persist($user);

            $this->addReference(self::class . '_' . $i, $user);
        }

        $manager->flush();
    }
}
