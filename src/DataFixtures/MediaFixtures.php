<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\MediaObject;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class MediaFixtures extends Fixture implements OrderedFixtureInterface
{
    use FixturesOrder;

    public const USER_AVATAR = 'user.png';
    public const PROJECT_IMAGE = 'project.png';
    public const POST_IMAGE = 'post.png';

    public const FILES = [
        self::USER_AVATAR,
        self::PROJECT_IMAGE,
        self::POST_IMAGE,
    ];

    /**
     * @var ParameterBagInterface
     */
    private $bag;

    /**
     * @param ParameterBagInterface $bag
     */
    public function __construct(ParameterBagInterface $bag)
    {
        $this->bag = $bag;
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager): void
    {
        $this->copyMedia();

        $this->media($manager, 'media_' . UserFixtures::class, UserFixtures::COUNT, self::USER_AVATAR);
        $this->media($manager, 'media_' . ProjectFixtures::class, ProjectFixtures::COUNT, self::PROJECT_IMAGE);

        $manager->flush();
    }

    /**
     * @param ObjectManager $manager
     * @param string        $referenceName
     * @param int           $count
     * @param string        $fileName
     */
    private function media(ObjectManager $manager, string $referenceName, int $count, string $fileName): void
    {
        for ($i = 0; $i < $count; $i++) {
            $media = new MediaObject();
            $media->setName($fileName);
            $manager->persist($media);

            $this->addReference($referenceName . '_' . $i, $media);
        }
    }

    private function copyMedia(): void
    {
        foreach (self::FILES as $fileName) {
            copy(__DIR__ . '/Files/' . $fileName, $this->bag->get('images_directory') . '/' . $fileName);
        }
    }
}
