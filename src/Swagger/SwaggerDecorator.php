<?php

declare(strict_types=1);

namespace App\Swagger;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

final class SwaggerDecorator implements NormalizerInterface
{
    /**
     * @var NormalizerInterface
     */
    private $decorated;

    /**
     * @param NormalizerInterface $decorated
     */
    public function __construct(NormalizerInterface $decorated)
    {
        $this->decorated = $decorated;
    }

    /**
     * {@inheritdoc}
     */
    public function normalize($object, string $format = null, array $context = [])
    {
        $docs = $this->decorated->normalize($object, $format, $context);

        unset(
            $docs['paths']['/api/users/{id}/followers/{followers}/followings/{following}/posts'],
            $docs['paths']['/api/users/{id}/followers/{followers}/posts'],
            $docs['paths']['/api/users/{id}/followings/{following}/followers/{followers}/posts'],
            $docs['paths']['/api/users/{id}/followings/{following}/posts'],
            $docs['paths']['/api/users/{id}/followers/{followers}/followings'],
            $docs['paths']['/api/users/{id}/followings/{following}/followers'],
        );

        $docs['components']['schemas']['UserNotification'] = [
            'type' => 'object',
            'properties' => [
                'hydra:member' => [
                    'type' => 'array',
                    'items' => [
                        'type' => 'object',
                        'properties' => [
                            'type' => [
                                'type' => 'string',
                                'readOnly' => true,
                            ],
                            'user' => [
                                'type' => 'object',
                                'properties' => [
                                    '@context' => [
                                        'type' => 'string',
                                        'readOnly' => true,
                                    ],
                                    '@id' => [
                                        'type' => 'string',
                                        'readOnly' => true,
                                    ],
                                    '@type' => [
                                        'type' => 'string',
                                        'readOnly' => true,
                                    ],
                                    'id' => [
                                        'type' => 'integer',
                                        'readOnly' => true,
                                    ],
                                    'email' => [
                                        'type' => 'string',
                                        'readOnly' => true,
                                    ],
                                    'nickname' => [
                                        'type' => 'string',
                                        'readOnly' => true,
                                    ],
                                    'fullName' => [
                                        'type' => 'string',
                                        'readOnly' => true,
                                    ],
                                    'avatar' => [
                                        'type' => 'object',
                                        'properties' => [
                                            '@id' => [
                                                'type' => 'string',
                                                'readOnly' => true,
                                            ],
                                            '@type' => [
                                                'type' => 'string',
                                                'readOnly' => true,
                                            ],
                                            'url' => [
                                                'type' => 'string',
                                                'readOnly' => true,
                                            ],
                                            'name' => [
                                                'type' => 'string',
                                                'readOnly' => true,
                                            ],
                                        ],
                                    ],
                                    'friend' => [
                                        'type' => 'boolean',
                                        'readOnly' => true,
                                    ],
                                ],
                            ],
                            'post' => [
                                'type' => 'object',
                                'properties' => [
                                    '@context' => [
                                        'type' => 'string',
                                        'readOnly' => true,
                                    ],
                                    '@id' => [
                                        'type' => 'string',
                                        'readOnly' => true,
                                    ],
                                    '@type' => [
                                        'type' => 'string',
                                        'readOnly' => true,
                                    ],
                                    'id' => [
                                        'type' => 'integer',
                                        'readOnly' => true,
                                    ],
                                    'images' => [
                                        'type' => 'array',
                                        'items' => [
                                            'type' => 'object',
                                            'properties' => [
                                                '@id' => [
                                                    'type' => 'string',
                                                    'readOnly' => true,
                                                ],
                                                '@type' => [
                                                    'type' => 'string',
                                                    'readOnly' => true,
                                                ],
                                                'url' => [
                                                    'type' => 'string',
                                                    'readOnly' => true,
                                                ],
                                                'name' => [
                                                    'type' => 'string',
                                                    'readOnly' => true,
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                            'createdAt' => [
                                'type' => 'string',
                                'readOnly' => true,
                            ],
                        ],
                    ],
                ],
                'hydra:totalItems' => [
                    'type' => 'integer',
                    'readOnly' => true,
                ],
            ],
        ];

        $docs['components']['schemas']['AuthenticationAdmin'] = [
            'type' => 'object',
            'properties' => [
                '@context' => [
                    'type' => 'string',
                    'readOnly' => true,
                ],
                '@id' => [
                    'type' => 'string',
                    'readOnly' => true,
                ],
                '@type' => [
                    'type' => 'string',
                    'readOnly' => true,
                ],
                'id' => [
                    'type' => 'integer',
                    'readOnly' => true,
                ],
                'email' => [
                    'type' => 'string',
                    'readOnly' => true,
                ],
                'nickname' => [
                    'type' => 'string',
                    'readOnly' => true,
                ],
                'accessToken' => [
                    'type' => 'string',
                    'readOnly' => true,
                ],
                'fullName' => [
                    'type' => 'string',
                    'readOnly' => true,
                ],
                'createdAt' => [
                    'type' => 'string',
                    'readOnly' => true,
                ],
                'updatedAt' => [
                    'type' => 'string',
                    'readOnly' => true,
                ],
            ],
        ];

        $docs['components']['schemas']['AuthenticationUser'] = [
            'type' => 'object',
            'properties' => [
                '@context' => [
                    'type' => 'string',
                    'readOnly' => true,
                ],
                '@id' => [
                    'type' => 'string',
                    'readOnly' => true,
                ],
                '@type' => [
                    'type' => 'string',
                    'readOnly' => true,
                ],
                'id' => [
                    'type' => 'integer',
                    'readOnly' => true,
                ],
                'email' => [
                    'type' => 'string',
                    'readOnly' => true,
                ],
                'nickname' => [
                    'type' => 'string',
                    'readOnly' => true,
                ],
                'accessToken' => [
                    'type' => 'string',
                    'readOnly' => true,
                ],
                'fullName' => [
                    'type' => 'string',
                    'readOnly' => true,
                ],
                'createdAt' => [
                    'type' => 'string',
                    'readOnly' => true,
                ],
                'updatedAt' => [
                    'type' => 'string',
                    'readOnly' => true,
                ],
                'avatar' => [
                    'type' => 'object',
                ],
                'countFollowers' => [
                    'type' => 'integer',
                    'readOnly' => true,
                ],
                'countFollowings' => [
                    'type' => 'integer',
                    'readOnly' => true,
                ],
            ],
        ];

        $docs['components']['schemas']['Credentials'] = [
            'type' => 'object',
            'properties' => [
                'email' => [
                    'type' => 'string',
                ],
                'password' => [
                    'type' => 'string',
                ],
            ],
        ];

        $docs['components']['schemas']['ResetPassword'] = [
            'type' => 'object',
            'properties' => [
                'email' => [
                    'type' => 'string',
                ],
            ],
        ];

        $docs['components']['schemas']['Unauthorized'] = [
            'type' => 'object',
            'properties' => [
                '@context' => [
                    'type' => 'string',
                    'readOnly' => true,
                ],
                '@type' => [
                    'type' => 'string',
                    'readOnly' => true,
                ],
                'hydra:title' => [
                    'type' => 'string',
                ],
                'hydra:description' => [
                    'type' => 'string',
                ],
            ],
        ];

        $docs['components']['schemas']['NotFound'] = [
            'type' => 'object',
            'properties' => [
                '@context' => [
                    'type' => 'string',
                    'readOnly' => true,
                ],
                '@type' => [
                    'type' => 'string',
                    'readOnly' => true,
                ],
                'hydra:title' => [
                    'type' => 'string',
                ],
                'hydra:description' => [
                    'type' => 'string',
                ],
            ],
        ];

        $docs['components']['schemas']['InvalidInput'] = [
            'type' => 'object',
            'properties' => [
                '@context' => [
                    'type' => 'string',
                    'readOnly' => true,
                ],
                '@type' => [
                    'type' => 'string',
                    'readOnly' => true,
                ],
                'hydra:title' => [
                    'type' => 'string',
                ],
                'hydra:description' => [
                    'type' => 'string',
                ],
                'violations' => [
                    'type' => 'array',
                    'items' => [
                        'type' => 'object',
                    ],
                ],
            ],
        ];

        $userNotificationDocumentation = [
            'get' => [
                'tags' => ['User'],
                'operationId' => 'userNotificationDocumentation',
                'summary' => 'Get user notifications.',
                'parameters' => [
                    [
                        'name' => 'id',
                        'in' => 'path',
                        'required' => 'true',
                        'type' => 'integer',
                    ],
                ],
                'responses' => [
                    Response::HTTP_OK => [
                        'description' => 'User notifications resource response',
                        'content' => [
                            'application/ld+json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/UserNotification',
                                ],
                            ],
                        ],
                    ],
                    Response::HTTP_UNAUTHORIZED => [
                        'description' => 'Unauthorized',
                        'content' => [
                            'application/ld+json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/Unauthorized',
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];

        $userTokenDocumentation = [
            'post' => [
                'tags' => ['Authentication'],
                'operationId' => 'postUserLoginItem',
                'summary' => 'Get user authentication token.',
                'requestBody' => [
                    'description' => 'Create new authentication token',
                    'content' => [
                        'application/ld+json' => [
                            'schema' => [
                                '$ref' => '#/components/schemas/Credentials',
                            ],
                        ],
                    ],
                ],
                'responses' => [
                    Response::HTTP_OK => [
                        'description' => 'User resource response',
                        'content' => [
                            'application/ld+json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/AuthenticationUser',
                                ],
                            ],
                        ],
                    ],
                    Response::HTTP_UNAUTHORIZED => [
                        'description' => 'Unauthorized',
                        'content' => [
                            'application/ld+json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/Unauthorized',
                                ],
                            ],
                        ],
                    ],
                    Response::HTTP_BAD_REQUEST => [
                        'description' => 'Invalid input',
                        'content' => [
                            'application/ld+json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/InvalidInput',
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];

        $adminTokenDocumentation = [
            'post' => [
                'tags' => ['Authentication'],
                'operationId' => 'postAdminLoginItem',
                'summary' => 'Get admin authentication token.',
                'requestBody' => [
                    'description' => 'Create new authentication token',
                    'content' => [
                        'application/ld+json' => [
                            'schema' => [
                                '$ref' => '#/components/schemas/Credentials',
                            ],
                        ],
                    ],
                ],
                'responses' => [
                    Response::HTTP_OK => [
                        'description' => 'User resource response',
                        'content' => [
                            'application/ld+json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/AuthenticationAdmin',
                                ],
                            ],
                        ],
                        Response::HTTP_UNAUTHORIZED => [
                            'description' => 'Unauthorized',
                            'content' => [
                                'application/ld+json' => [
                                    'schema' => [
                                        '$ref' => '#/components/schemas/Unauthorized',
                                    ],
                                ],
                            ],
                        ],
                        Response::HTTP_BAD_REQUEST => [
                            'description' => 'Invalid input',
                            'content' => [
                                'application/ld+json' => [
                                    'schema' => [
                                        '$ref' => '#/components/schemas/InvalidInput',
                                    ],
                                ],
                            ],
                        ],
                    ], ],
            ],
        ];

        $resetPasswordDocumentation = [
            'post' => [
                'tags' => ['Authentication'],
                'operationId' => 'resetPassword',
                'summary' => 'Reset user password.',
                'requestBody' => [
                    'description' => 'Reset password data',
                    'content' => [
                        'application/ld+json' => [
                            'schema' => [
                                '$ref' => '#/components/schemas/ResetPassword',
                            ],
                        ],
                    ],
                ],
                'responses' => [
                    Response::HTTP_OK => [
                        'description' => 'Email send success',
                        'content' => [
                            'application/ld+json' => [],
                        ],
                    ],
                    Response::HTTP_BAD_REQUEST => [
                        'description' => 'Invalid input',
                        'content' => [
                            'application/ld+json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/InvalidInput',
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];

        $logoutDocumentation = [
            'get' => [
                'tags' => ['Authentication'],
                'operationId' => 'getLogout',
                'summary' => 'Logout.',
                'requestBody' => [
                    'description' => 'Logout',
                ],
                'responses' => [
                    Response::HTTP_OK => [
                        'description' => 'Logout success',
                    ],
                    Response::HTTP_UNAUTHORIZED => [
                        'description' => 'Unauthorized',
                        'content' => [
                            'application/ld+json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/Unauthorized',
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];

        $tokenDocumentation = [
            'paths' => [
                '/api/users/login' => $userTokenDocumentation,
                '/api/admin/login' => $adminTokenDocumentation,
                '/api/logout' => $logoutDocumentation,
                '/api/reset_password' => $resetPasswordDocumentation,
            ],
        ];

        foreach ($docs['paths'] as &$path) {
            foreach ($path as $type => &$request) {
                if (isset($request['responses'][Response::HTTP_BAD_REQUEST])) {
                    $request['responses'][Response::HTTP_BAD_REQUEST]['content']['application/ld+json']['schema'] = [
                        '$ref' => '#/components/schemas/InvalidInput',
                    ];
                }
                if (isset($request['responses'][Response::HTTP_NOT_FOUND])) {
                    $request['responses'][Response::HTTP_NOT_FOUND]['content']['application/ld+json']['schema'] = [
                        '$ref' => '#/components/schemas/NotFound',
                    ];
                }
            }
        }

        $docs['paths']['/api/users/{id}/notifications'] = $userNotificationDocumentation;

        $docs['paths']['/api/users/registration']['post']['tags'] = ['Authentication'];

        return array_merge_recursive($docs, $tokenDocumentation);
    }

    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, string $format = null): bool
    {
        return $this->decorated->supportsNormalization($data, $format);
    }
}
