<?php

declare(strict_types=1);

namespace App\Entity\Project;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Entity\Category\Category;
use App\Entity\MediaObject;
use App\Entity\Post\Post;
use App\Entity\User\User;
use App\Repository\Project\ProjectRepository;
use App\Service\Timestamp\AutoTimestampTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ProjectRepository::class)
 * @ORM\HasLifecycleCallbacks
 * @ApiResource(
 *     attributes={"pagination_enabled" = false},
 *     collectionOperations={
 *         "get" = {
 *             "normalization_context" = {"groups" = {"project:list"}}
 *         },
 *         "post" = {
 *             "normalization_context" = {"groups" = {"project:item"}},
 *             "denormalization_context" = {"groups" = {"project:post"}},
 *             "validation_groups" = {"project:post"},
 *             "security" = "is_granted('ROLE_ADMIN')",
 *         }
 *     },
 *     itemOperations={
 *         "get" = {
 *             "normalization_context" = {"groups" = {"project:item"}}
 *         },
 *         "put" = {
 *             "normalization_context" = {"groups" = {"project:item"}},
 *             "denormalization_context" = {"groups" = {"project:put"}},
 *             "validation_groups" = {"project:put"},
 *             "security" = "is_granted('ROLE_ADMIN')",
 *         },
 *         "delete" = {
 *             "security" = "is_granted('ROLE_ADMIN')",
 *         }
 *     },
 *     attributes={"order" = {"updatedAt" : "DESC"}}
 * )
 */
class Project
{
    use AutoTimestampTrait;

    /**
     * @var int|null
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"project:item", "project:list", "post:item", "post:list", "category:item", "category:list"})
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=100)
     * @Groups({"project:item", "project:list", "project:post", "project:put", "post:item", "post:list", "category:item", "category:list"})
     * @ApiFilter(SearchFilter::class, strategy="partial")
     */
    private $title;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"project:item", "project:list", "project:post", "project:put", "category:item", "category:list"})
     */
    private $place;

    /**
     * @var int
     * @ORM\Column(type="integer")
     * @Groups({"project:item", "project:list", "post:item", "post:list", "category:item", "category:list"})
     */
    private $collected = 0;

    /**
     * @var int
     * @ORM\Column(type="integer")
     * @Groups({"project:item", "project:list", "project:post", "project:put", "post:item", "post:list", "category:item", "category:list"})
     */
    private $collectionAmount;

    /**
     * @var \DateTimeInterface
     * @ORM\Column(type="datetime")
     * @Groups({"project:item", "project:list", "project:post", "project:put", "category:item", "category:list"})
     */
    private $startAt;

    /**
     * @var \DateTimeInterface
     * @ORM\Column(type="datetime")
     * @Groups({"project:item", "project:list", "project:post", "project:put", "category:item", "category:list"})
     */
    private $endAt;

    /**
     * @var string
     * @ORM\Column(type="text")
     * @Groups({"project:item", "project:list", "project:post", "project:put", "category:item", "category:list"})
     */
    private $description;

    /**
     * @var \DateTimeInterface
     * @ORM\Column(type="datetime")
     * @Groups({"project:item", "project:list", "category:item", "category:list"})
     */
    private $createdAt;

    /**
     * @var \DateTimeInterface
     * @ORM\Column(type="datetime")
     * @Groups({"project:item", "project:list", "c/home/noi/projects/marketplace-backend/bin/consoleategory:item", "category:list"})
     */
    private $updatedAt;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="projects")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"project:item", "project:list"})
     */
    private $user;

    /**
     * @var MediaObject
     * @ORM\OneToOne(targetEntity=MediaObject::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"project:item", "project:list", "project:post", "project:put", "category:item", "category:list"})
     * @Assert\NotBlank(
     *     message="This value should be IRI string. Like '/api/{resource}/{id}'",
     *     groups={"project:post", "project:put"}
     * )
     */
    private $image;

    /**
     * @var int
     * @Groups({"project:item", "project:list"})
     */
    private $uniqueDonationsCount = 0;

    /**
     * @var Collection|Post[]
     * @ORM\OneToMany(targetEntity=Post::class, mappedBy="project")
     * @Groups({"project:item", "project:list"})
     */
    private $posts;

    /**
     * @var Category
     * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="projects")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"project:item", "project:list", "project:post", "project:put"})
     * @ApiFilter(SearchFilter::class, strategy="exact", properties={"category.name"})
     */
    private $category;

    public function __construct()
    {
        $this->posts = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Project
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPlace(): ?string
    {
        return $this->place;
    }

    /**
     * @param string|null $place
     * @return Project
     */
    public function setPlace(?string $place): self
    {
        $this->place = $place;

        return $this;
    }

    /**
     * @return int
     */
    public function getCollected(): int
    {
        return $this->collected;
    }

    /**
     * @param int $collected
     * @return Project
     */
    public function setCollected(int $collected): self
    {
        $this->collected = $collected;

        return $this;
    }

    /**
     * @return int
     */
    public function getCollectionAmount(): int
    {
        return $this->collectionAmount;
    }

    /**
     * @param int $collectionAmount
     * @return Project
     */
    public function setCollectionAmount(int $collectionAmount): self
    {
        $this->collectionAmount = $collectionAmount;

        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getStartAt(): \DateTimeInterface
    {
        return $this->startAt;
    }

    /**
     * @param \DateTimeInterface $startAt
     * @return Project
     */
    public function setStartAt(\DateTimeInterface $startAt): self
    {
        $this->startAt = $startAt;

        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getEndAt(): \DateTimeInterface
    {
        return $this->endAt;
    }

    /**
     * @param \DateTimeInterface $endAt
     * @return Project
     */
    public function setEndAt(\DateTimeInterface $endAt): self
    {
        $this->endAt = $endAt;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Project
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return Project
     */
    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return MediaObject
     */
    public function getImage(): MediaObject
    {
        return $this->image;
    }

    /**
     * @param MediaObject $image
     * @return Project
     */
    public function setImage(MediaObject $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return Collection|Post[]
     */
    public function getPosts(): Collection
    {
        return $this->posts;
    }

    /**
     * @param Post $post
     * @return $this
     */
    public function addPost(Post $post): self
    {
        if (!$this->posts->contains($post)) {
            $this->posts[] = $post;
            $post->setProject($this);
        }

        return $this;
    }

    /**
     * @param Post $post
     * @return $this
     */
    public function removePost(Post $post): self
    {
        if ($this->posts->contains($post)) {
            $this->posts->removeElement($post);
            // set the owning side to null (unless already changed)
            if ($post->getProject() === $this) {
                $post->setProject(null);
            }
        }

        return $this;
    }

    /**
     * @return Category
     */
    public function getCategory(): Category
    {
        return $this->category;
    }

    /**
     * @param Category $category
     * @return Project
     */
    public function setCategory(Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return int
     */
    public function getUniqueDonationsCount(): int
    {
        return $this->uniqueDonationsCount;
    }

    /**
     * @param int $uniqueDonationsCount
     * @return Project
     */
    public function setUniqueDonationsCount(int $uniqueDonationsCount): self
    {
        $this->uniqueDonationsCount = $uniqueDonationsCount;

        return $this;
    }
}
