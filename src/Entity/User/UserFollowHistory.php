<?php

declare(strict_types=1);

namespace App\Entity\User;

use App\Repository\User\UserFollowHistoryRepository;
use App\Service\Timestamp\DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass=UserFollowHistoryRepository::class)
 */
class UserFollowHistory
{
    /**
     * @var int|null
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"user:item"})
     */
    private $id;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="userFollowerHistories")
     * @ORM\JoinColumn(nullable=false)
     */
    private $follower;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="userFollowerHistories")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"user:item"})
     */
    private $following;

    /**
     * @var \DateTimeInterface
     * @ORM\Column(type="datetime")
     * @Groups({"user:item"})
     */
    private $createdAt;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getFollower(): User
    {
        return $this->follower;
    }

    /**
     * @param User $follower
     * @return UserFollowHistory
     */
    public function setFollower(User $follower): self
    {
        $this->follower = $follower;

        return $this;
    }

    /**
     * @return User
     */
    public function getFollowing(): User
    {
        return $this->following;
    }

    /**
     * @param User $following
     * @return UserFollowHistory
     */
    public function setFollowing(User $following): self
    {
        $this->following = $following;

        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getCreatedAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTimeInterface $createdAt
     * @return UserFollowHistory
     */
    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @ORM\PrePersist
     * @throws \Exception
     */
    public function dateTime(): void
    {
        $this->setCreatedAt(DateTime::getDateTime());
    }
}
