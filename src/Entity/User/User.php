<?php

declare(strict_types=1);

namespace App\Entity\User;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Controller\User\UserFollowingAction;
use App\Controller\User\UserUnFollowingAction;
use App\Controller\User\UserUpdatePointsAction;
use App\Entity\MediaObject;
use App\Entity\Post\Post;
use App\Entity\Post\PostUserSmile;
use App\Entity\Project\Project;
use App\Repository\User\UserRepository;
use App\Service\Timestamp\AutoTimestampTrait;
use App\Validator\Constraints as AcmeAssert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ApiResource(
 *     normalizationContext={"groups" = {"user:list"}},
 *     collectionOperations={
 *         "get" = {
 *             "normalization_context" = {"groups" = {"user:list"}}
 *         },
 *         "post" = {
 *             "path" = "/users/registration",
 *             "normalization_context" = {"groups" = {"user:item:post"}},
 *             "denormalization_context" = {"groups" = {"user:post"}},
 *             "validation_groups" = {"user:post"},
 *         },
 *         "notifications" = {
 *             "pagination_enabled" = false,
 *             "normalization_context" = {"groups" = {"user:notifications"}},
 *             "security" = "object == user or is_granted('ROLE_ADMIN')",
 *             "route_name" = "user_notifications"
 *         }
 *     },
 *     itemOperations={
 *         "get" = {
 *             "normalization_context" = {"groups" = {"user:item"}},
 *         },
 *         "put" = {
 *             "normalization_context" = {"groups" = {"user:item"}},
 *             "denormalization_context" = {"groups" = {"user:put"}},
 *             "validation_groups" = {"user:post"},
 *             "security" = "object == user",
 *         },
 *         "delete" = {
 *             "security" = "object == user",
 *         },
 *         "following" = {
 *             "normalization_context" = {"groups" = {"user:item"}},
 *             "denormalization_context" = {"groups" = {}},
 *             "method" = "PUT",
 *             "path" = "/users/{id}/following",
 *             "controller" = UserFollowingAction::class,
 *             "openapi_context" = {
 *                 "summary" = "Subscribe to User.",
 *             }
 *         },
 *         "unfollowing" = {
 *             "normalization_context" = {"groups" = {"user:item"}},
 *             "denormalization_context" = {"groups" = {}},
 *             "method" = "PUT",
 *             "path" = "/users/{id}/unfollowing",
 *             "controller" = UserUnFollowingAction::class,
 *             "openapi_context" = {
 *                 "summary" = "Describe from User.",
 *             }
 *         },
 *         "change_password" = {
 *             "method" = "PUT",
 *             "path" = "/users/{id}/change_password",
 *             "normalization_context" = {"groups" = {"user:item"}},
 *             "denormalization_context" = {"groups" = {"change-password:put"}},
 *             "validation_groups" = {"change-password"},
 *             "access_control" = "object == user",
 *             "openapi_context" = {
 *                 "summary" = "Replace User password.",
 *             }
 *         },
 *         "get_points" = {
 *             "method" = "GET",
 *             "path" = "/users/{id}/points",
 *             "normalization_context" = {"groups" = {"user:points:item"}},
 *             "security" = "object == user or is_granted('ROLE_ADMIN')",
 *             "openapi_context" = {
 *                 "summary" = "Get User points.",
 *             }
 *         },
 *         "post_points" = {
 *             "method" = "POST",
 *             "path" = "/users/{id}/points",
 *             "controller" = UserUpdatePointsAction::class,
 *             "normalization_context" = {"groups" = {"user:points:item"}},
 *             "denormalization_context" = {"groups" = {"user:points:post"}},
 *             "validation_groups" = {"user:points"},
 *             "security" = "object == user or is_granted('ROLE_ADMIN')",
 *             "openapi_context" = {
 *                 "summary" = "Top up User points.",
 *             }
 *         },
 *     },
 *     subresourceOperations={
 *         "api_users_followers_get_subresource" = {
 *             "method" = "GET",
 *             "normalization_context" = {"groups" = {"user:item"}},
 *         },
 *         "api_users_followings_get_subresource" = {
 *             "method" = "GET",
 *             "normalization_context" = {"groups" = {"user:item"}},
 *         }
 *     }
 * )
 * @UniqueEntity(fields={"email"}, message="User with this email already exists", groups={"user:post"})
 * @UniqueEntity(fields={"nickname"}, message="Nickname already exists", groups={"user:post"})
 * @UniqueEntity(fields={"avatar"}, message="Avatar already in use", groups={"user:post"})
 */
class User implements UserInterface
{
    use AutoTimestampTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"user:item", "user:list", "user:login", "admin:login", "user:item:post", "post:item", "post:list", "post-comment:item", "post-comment:list", "project:item", "project:list", "post-user-smile:list:user", "user:notifications"})
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(type="integer")
     * @Groups({"user:points:item", "user:points:self"})
     */
    private $points = 0;

    /**
     * @var int
     * @Groups({"user:points:post"})
     * @Assert\Positive(
     *     groups={"user:points"},
     *     message="Point must be greater than 0"
     * )
     * @SerializedName("points")
     */
    private $pointsUp;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Groups({"user:item", "user:list", "user:login", "admin:login", "user:item:post", "user:post", "post:item", "post:list", "post-comment:item", "post-comment:list", "project:item", "project:list", "post-user-smile:list:user", "user:notifications"})
     * @Assert\Email(
     *     message="The email '{{ value }}' is not a valid email.",
     *     groups={"user:post", "user:reset-password"}
     * )
     * @Assert\Length(
     *     max="180",
     *     groups={"user:post", "user:reset-password"}
     * )
     */
    private $email;

    /**
     * @var string
     * @ORM\Column(type="string", length=50, unique=true)
     * @Groups({"user:item", "user:list", "user:login", "admin:login", "user:put", "user:post", "user:item:post", "post:item", "post:list", "post-comment:item", "post-comment:list", "project:item", "project:list", "post-user-smile:list:user", "user:notifications"})
     * @Assert\NotBlank(groups={"user:post"})
     * @Assert\Length(
     *     min="5",
     *     max="50",
     *     groups={"user:post"}
     * )
     * @ApiFilter(SearchFilter::class, strategy="partial")
     */
    private $nickname;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"user:item", "user:list", "user:item:post", "user:post", "user:put", "user:login", "admin:login", "post:item", "post:list", "project:item", "project:list", "post-user-smile:list:user", "user:notifications"})
     * @Assert\NotBlank(groups={"user:post"})
     * @Assert\Length(
     *     max="255",
     *     groups={"user:post"}
     * )
     * @ApiFilter(SearchFilter::class, strategy="partial")
     */
    private $fullName;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     * @Groups({"user:post"})
     * @Assert\Length(
     *     min=6,
     *     groups={"user:post"}
     * )
     */
    private $password;

    /**
     * @var string|null
     * @Groups({"change-password:put"})
     * @Assert\NotNull(groups={"change-password"})
     * @Assert\NotBlank(groups={"change-password"})
     * @AcmeAssert\UserPassword(groups={"change-password"})
     */
    private $oldPassword;

    /**
     * @var string|null
     * @Groups({"change-password:put"})
     * @Assert\NotNull(groups={"change-password"})
     * @Assert\Length(
     *     min=6,
     *     groups={"change-password"}
     * )
     */
    private $newPassword;

    /**
     * @var string|null
     * @Groups({"change-password:put"})
     * @Assert\IdenticalTo(
     *     propertyPath="newPassword",
     *     groups={"change-password"},
     *     message="This value should be identical to newPassword."
     * )
     */
    private $repeatNewPassword;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     * @Groups({"user:item:post", "user:login", "admin:login"})
     */
    private $accessToken;

    /**
     * @ORM\OneToMany(targetEntity=Project::class, mappedBy="user", orphanRemoval=true)
     */
    private $projects;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"user:item", "user:list", "user:item:post", "user:login", "admin:login", "project:item", "project:list", "post-user-smile:list:user"})
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"user:item", "user:list", "user:item:post", "user:login", "admin:login", "project:item", "project:list", "post-user-smile:list:user"})
     */
    private $updatedAt;

    /**
     * @ORM\OneToOne(targetEntity=MediaObject::class, cascade={"persist", "remove"})
     * @Groups({"user:login", "user:item", "user:list", "user:put", "post:item", "post:list", "project:item", "project:list", "post-user-smile:list:user", "user:notifications"})
     */
    private $avatar;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="following")
     * @ApiSubresource(maxDepth=1)
     */
    private $followers;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, mappedBy="followers")
     * @ApiSubresource(maxDepth=1)
     */
    private $following;

    /**
     * @var bool|null
     * @Groups({"user:item", "user:list", "user:notifications"})
     */
    private $friend;

    /**
     * @var int
     * @Groups({"user:login", "user:item", "user:list"})
     */
    private $countDonations = 0;

    /**
     * @var int
     * @Groups({"user:login", "user:item", "user:list"})
     */
    private $countFollowers = 0;

    /**
     * @var int
     * @Groups({"user:login", "user:item", "user:list"})
     */
    private $countFollowings = 0;

    /**
     * @var Collection|Post[]
     * @ORM\OneToMany(targetEntity=Post::class, mappedBy="user", orphanRemoval=true)
     * @ApiSubresource(maxDepth=1)
     */
    private $posts;

    /**
     * @var Collection|PostUserSmile[]
     * @ORM\OneToMany(targetEntity=PostUserSmile::class, mappedBy="user", orphanRemoval=true)
     */
    private $postUserSmiles;

    /**
     * @var Collection|User[]
     * @ORM\OneToMany(targetEntity=UserFollowHistory::class, mappedBy="follower", orphanRemoval=true)
     * @ORM\OrderBy({"createdAt" : "DESC"})
     */
    private $userFollowerHistories;

    public function __construct()
    {
        $this->projects = new ArrayCollection();
        $this->followers = new ArrayCollection();
        $this->following = new ArrayCollection();
        $this->posts = new ArrayCollection();
        $this->postUserSmiles = new ArrayCollection();
        $this->userFollowerHistories = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getPoints(): int
    {
        return $this->points;
    }

    /**
     * @param int $points
     * @return User
     */
    public function setPoints(int $points): self
    {
        $this->points = $points;

        return $this;
    }

    /**
     * @return int
     */
    public function getPointsUp(): int
    {
        return $this->pointsUp;
    }

    /**
     * @param int $pointsUp
     * @return User
     */
    public function setPointsUp(int $pointsUp): self
    {
        $this->pointsUp = $pointsUp;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getOldPassword(): ?string
    {
        return $this->oldPassword;
    }

    /**
     * @param string|null $oldPassword
     * @return User
     */
    public function setOldPassword(?string $oldPassword): self
    {
        $this->oldPassword = $oldPassword;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getNewPassword(): ?string
    {
        return $this->newPassword;
    }

    /**
     * @param string|null $newPassword
     * @return User
     */
    public function setNewPassword(?string $newPassword): self
    {
        $this->newPassword = $newPassword;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getRepeatNewPassword(): ?string
    {
        return $this->repeatNewPassword;
    }

    /**
     * @param string|null $repeatNewPassword
     * @return User
     */
    public function setRepeatNewPassword(?string $repeatNewPassword): self
    {
        $this->repeatNewPassword = $repeatNewPassword;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt(): void
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getAccessToken(): ?string
    {
        return $this->accessToken;
    }

    public function setAccessToken(?string $accessToken): self
    {
        $this->accessToken = $accessToken;

        return $this;
    }

    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    public function setFullName(string $fullName): self
    {
        $this->fullName = $fullName;

        return $this;
    }

    /**
     * @return Collection|Project[]
     */
    public function getProjects(): Collection
    {
        return $this->projects;
    }

    public function addProject(Project $project): self
    {
        if (!$this->projects->contains($project)) {
            $this->projects[] = $project;
            $project->setUser($this);
        }

        return $this;
    }

    public function removeProject(Project $project): self
    {
        if ($this->projects->contains($project)) {
            $this->projects->removeElement($project);
            // set the owning side to null (unless already changed)
            if ($project->getUser() === $this) {
                $project->setUser(null);
            }
        }

        return $this;
    }

    public function getAvatar(): ?MediaObject
    {
        return $this->avatar;
    }

    public function setAvatar(?MediaObject $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getFollowers(): Collection
    {
        return $this->followers;
    }

    public function addFollower(self $follower): self
    {
        if (!$this->followers->contains($follower)) {
            $this->followers[] = $follower;
        }

        return $this;
    }

    public function removeFollower(self $follower): self
    {
        if ($this->followers->contains($follower)) {
            $this->followers->removeElement($follower);
        }

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getFollowing(): Collection
    {
        return $this->following;
    }

    public function addFollowing(self $following): self
    {
        if (!$this->following->contains($following)) {
            $this->following[] = $following;
            $following->addFollower($this);
        }

        return $this;
    }

    public function removeFollowing(self $following): self
    {
        if ($this->following->contains($following)) {
            $this->following->removeElement($following);
            $following->removeFollower($this);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getNickname(): string
    {
        return $this->nickname;
    }

    /**
     * @param string $nickname
     * @return User
     */
    public function setNickname(string $nickname): self
    {
        $this->nickname = $nickname;

        return $this;
    }

    /**
     * @return int
     */
    public function getCountDonations(): int
    {
        return $this->countDonations;
    }

    /**
     * @param int $countDonations
     * @return User
     */
    public function setCountDonations(int $countDonations): self
    {
        $this->countDonations = $countDonations;

        return $this;
    }

    /**
     * @return int
     */
    public function getCountFollowers(): int
    {
        return $this->countFollowers;
    }

    /**
     * @param int $countFollowers
     * @return User
     */
    public function setCountFollowers(int $countFollowers): self
    {
        $this->countFollowers = $countFollowers;

        return $this;
    }

    /**
     * @return int
     */
    public function getCountFollowings(): int
    {
        return $this->countFollowings;
    }

    /**
     * @param int $countFollowings
     * @return User
     */
    public function setCountFollowings(int $countFollowings): self
    {
        $this->countFollowings = $countFollowings;

        return $this;
    }

    /**
     * @return Collection|Post[]
     */
    public function getPosts(): Collection
    {
        return $this->posts;
    }

    /**
     * @param Post $post
     * @return $this
     */
    public function addPost(Post $post): self
    {
        if (!$this->posts->contains($post)) {
            $this->posts[] = $post;
            $post->setUser($this);
        }

        return $this;
    }

    /**
     * @param Post $post
     * @return $this
     */
    public function removePost(Post $post): self
    {
        if ($this->posts->contains($post)) {
            $this->posts->removeElement($post);
            // set the owning side to null (unless already changed)
            if ($post->getUser() === $this) {
                $post->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getFriend(): ?bool
    {
        return $this->friend;
    }

    /**
     * @param bool|null $friend
     * @return User
     */
    public function setFriend(?bool $friend): self
    {
        $this->friend = $friend;

        return $this;
    }

    /**
     * @return Collection|PostUserSmile[]
     */
    public function getPostUserSmiles(): Collection
    {
        return $this->postUserSmiles;
    }

    /**
     * @param PostUserSmile $postUserSmile
     * @return $this
     */
    public function addPostUserSmile(PostUserSmile $postUserSmile): self
    {
        if (!$this->postUserSmiles->contains($postUserSmile)) {
            $this->postUserSmiles[] = $postUserSmile;
            $postUserSmile->setUser($this);
        }

        return $this;
    }

    /**
     * @param PostUserSmile $postUserSmile
     * @return $this
     */
    public function removePostUserSmile(PostUserSmile $postUserSmile): self
    {
        if ($this->postUserSmiles->contains($postUserSmile)) {
            $this->postUserSmiles->removeElement($postUserSmile);
            // set the owning side to null (unless already changed)
            if ($postUserSmile->getUser() === $this) {
                $postUserSmile->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserFollowHistory[]
     */
    public function getUserFollowerHistories(): Collection
    {
        return $this->userFollowerHistories;
    }

    /**
     * @param UserFollowHistory $userFollowerHistory
     * @return $this
     */
    public function addUserFollowerHistory(UserFollowHistory $userFollowerHistory): self
    {
        if (!$this->userFollowerHistories->contains($userFollowerHistory)) {
            $this->userFollowerHistories[] = $userFollowerHistory;
            $userFollowerHistory->setFollower($this);
        }

        return $this;
    }

    /**
     * @param UserFollowHistory $userFollowerHistory
     * @return $this
     */
    public function removeUserFollowerHistory(UserFollowHistory $userFollowerHistory): self
    {
        if ($this->userFollowerHistories->contains($userFollowerHistory)) {
            $this->userFollowerHistories->removeElement($userFollowerHistory);
        }

        return $this;
    }
}
