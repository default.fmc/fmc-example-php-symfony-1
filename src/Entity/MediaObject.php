<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\CreateMediaObjectAction;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity
 * @ApiResource(
 *     normalizationContext={
 *         "groups" = {"media_object_read"}
 *     },
 *     collectionOperations={
 *         "post" = {
 *             "controller" = CreateMediaObjectAction::class,
 *             "deserialize" = false,
 *             "validation_groups" = {"media_object_create"},
 *             "openapi_context" = {
 *                 "requestBody" = {
 *                     "content" = {
 *                         "multipart/form-data" = {
 *                             "schema" = {
 *                                 "type" = "object",
 *                                 "properties" = {
 *                                     "file" = {
 *                                         "type" = "string",
 *                                         "format" = "binary"
 *                                     }
 *                                 }
 *                             }
 *                         }
 *                     }
 *                 }
 *             }
 *         },
 *         "get"
 *     },
 *     itemOperations={
 *         "get"
 *     }
 * )
 * @Vich\Uploadable
 */
class MediaObject
{
    /**
     * @var int|null
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     * @ORM\Id
     */
    private $id;

    /**
     * @var string|null
     *
     * @ApiProperty(iri="http://schema.org/url")
     * @Groups({"media_object_read", "project:item", "project:list", "user:login", "user:item", "user:list", "post:item", "post:list", "post-comment:item", "post-comment:list", "category:item", "category:list", "post-user-smile:list:user", "user:notifications"})
     */
    private $url;

    /**
     * @var File|null
     *
     * @Assert\NotNull(groups={"media_object_create"})
     * @Assert\File(
     *     maxSize="10m",
     *     mimeTypes={"image/gif", "image/jpeg", "image/pjpeg", "image/png", "image/tiff", "image/vnd.wap.wbmp", "image/webp"},
     *     mimeTypesMessage="This file type {{ type }} is invalid! Please upload a valid types {{ types }}"
     * )
     * @Vich\UploadableField(mapping="media_object", fileNameProperty="name")
     */
    private $file;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50, unique=false)
     * @Groups({"media_object_read", "project:item", "project:list", "user:login", "user:item", "user:list", "post:item", "post:list", "post-comment:item", "post-comment:list", "category:item", "category:list", "post-user-smile:list:user", "user:notifications"})
     */
    private $name;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @param string|null $url
     * @return MediaObject
     */
    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return File|null
     */
    public function getFile(): ?File
    {
        return $this->file;
    }

    /**
     * @param File|null $file
     * @return MediaObject
     */
    public function setFile(?File $file): self
    {
        $this->file = $file;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return MediaObject
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
