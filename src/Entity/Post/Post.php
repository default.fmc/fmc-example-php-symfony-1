<?php

declare(strict_types=1);

namespace App\Entity\Post;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Controller\Post\SmilePostAction;
use App\Controller\Post\UnSmilePostAction;
use App\Entity\MediaObject;
use App\Entity\Project\Project;
use App\Entity\User\User;
use App\Repository\Post\PostRepository;
use App\Service\Timestamp\AutoTimestampTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass=PostRepository::class)
 * @ApiResource(
 *     normalizationContext={"groups" = {"post:list"}},
 *     collectionOperations={
 *         "get" = {
 *             "normalization_context" = {"groups" = {"post:list"}}
 *         },
 *         "post" = {
 *             "normalization_context" = {"groups" = {"post:item:post"}},
 *             "denormalization_context" = {"groups" = {"post:post"}},
 *             "validation_groups" = {"post:post"},
 *             "security" = "is_granted('ROLE_USER')",
 *         }
 *     },
 *     itemOperations={
 *         "get" = {
 *             "normalization_context" = {"groups" = {"post:item"}},
 *         },
 *         "delete" = {
 *             "security" = "object.getUser() == user",
 *         },
 *         "smile" = {
 *             "normalization_context" = {"groups" = {"post:item"}},
 *             "denormalization_context" = {"groups" = {}},
 *             "method" = "PUT",
 *             "path" = "/posts/{id}/smile",
 *             "controller" = SmilePostAction::class,
 *             "openapi_context" = {
 *                 "summary" = "Smile (like) post.",
 *             }
 *         },
 *         "un_smile" = {
 *             "normalization_context" = {"groups" = {"post:item"}},
 *             "denormalization_context" = {"groups" = {}},
 *             "method" = "PUT",
 *             "path" = "/posts/{id}/un_smile",
 *             "controller" = UnSmilePostAction::class,
 *             "openapi_context" = {
 *                 "summary" = "Unsmile (unlike) post.",
 *             }
 *         }
 *     },
 *     attributes={"order" = {"updatedAt" : "DESC"}}
 * )
 */
class Post
{
    use AutoTimestampTrait;

    /**
     * @var int|null
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"post:item", "post:list", "post:item:post", "post-comment:item", "post-comment:list", "project:item", "project:list", "post-user-smile:list:post", "user:notifications"})
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     * @Groups({"post:item", "post:list", "post:item:post", "post:post", "post-comment:item", "post-comment:list", "project:item", "project:list", "post-user-smile:list:post", "user:notifications"})
     * @ApiFilter(SearchFilter::class, strategy="partial")
     */
    private $caption;

    /**
     * @var int
     * @ORM\Column(type="integer")
     * @Groups({"post:item", "post:list", "post:item:post", "post:post", "post-comment:item", "post-comment:list", "project:item", "project:list", "post-user-smile:list:post"})
     * @Assert\Positive(
     *     groups={"post:post"},
     *     message="Donation sum must be greater than 0"
     * )
     */
    private $donationSum;

    /**
     * @var int
     * @Groups({"post:item", "post:list", "post-comment:item", "post-comment:list", "project:item", "project:list", "post-user-smile:list:post"})
     */
    private $smiles = 0;

    /**
     * @var bool
     * @Groups({"post:item", "post:list"})
     */
    private $isSmile;

    /**
     * @var \DateTimeInterface
     * @ORM\Column(type="datetime")
     * @Groups({"post:item", "post:list", "project:item", "project:list", "post-user-smile:list:post"})
     */
    private $createdAt;

    /**
     * @var \DateTimeInterface
     * @ORM\Column(type="datetime")
     * @Groups({"post:item", "post:list", "project:item", "project:list", "post-user-smile:list:post"})
     */
    private $updatedAt;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="posts")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"post:item", "post:list", "project:item", "project:list", "post-user-smile:list:post"})
     */
    private $user;

    /**
     * @var Collection|MediaObject[]
     * @ORM\ManyToMany(targetEntity=MediaObject::class)
     * @Groups({"post:item", "post:list", "post:post", "post:item:post", "post-comment:item", "post-comment:list", "post-user-smile:list:post", "user:notifications"})
     * @Assert\Count(
     *     min=1,
     *     max=5,
     *     groups={"post:post"}
     * )
     */
    private $images;

    /**
     * @var int
     * @Groups({"post:item", "post:list"})
     */
    private $countComments = 0;

    /**
     * @var Collection|PostComment[]
     * @ORM\OneToMany(targetEntity=PostComment::class, mappedBy="post", orphanRemoval=true)
     * @Groups({"post:item", "post:list"})
     * @ApiSubresource(maxDepth=1)
     */
    private $comments;

    /**
     * @var Project
     * @ORM\ManyToOne(targetEntity=Project::class, inversedBy="posts")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"post:item", "post:list", "post:post", "post:item:post", "post-user-smile:list:post"})
     * @Assert\NotBlank(
     *     message="This value should be IRI string. Like '/api/{resource}/{id}'",
     *     groups={"post:post"}
     * )
     */
    private $project;

    /**
     * @var Collection|PostUserSmile[]
     * @ORM\OneToMany(targetEntity=PostUserSmile::class, mappedBy="post", orphanRemoval=true, fetch="EXTRA_LAZY")
     * @ORM\OrderBy({"createdAt" : "desc"})
     * @Groups({"post:smiles"})
     * @SerializedName("smiles")
     */
    private $postUserSmiles;

    public function __construct()
    {
        $this->images = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->postUserSmiles = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCaption(): string
    {
        return $this->caption;
    }

    /**
     * @return int
     */
    public function getDonationSum(): int
    {
        return $this->donationSum;
    }

    /**
     * @param int $donationSum
     * @return Post
     */
    public function setDonationSum(int $donationSum): self
    {
        $this->donationSum = $donationSum;

        return $this;
    }

    /**
     * @return int
     */
    public function getSmiles(): int
    {
        return $this->getPostUserSmiles()->count();
    }

    /**
     * @param int $smiles
     * @return Post
     */
    public function setSmiles(int $smiles): self
    {
        $this->smiles = $smiles;

        return $this;
    }

    /**
     * @return bool
     */
    public function getIsSmile(): bool
    {
        return $this->isSmile;
    }

    /**
     * @param bool $isSmile
     * @return Post
     */
    public function setIsSmile(bool $isSmile): self
    {
        $this->isSmile = $isSmile;

        return $this;
    }

    /**
     * @param User $user
     * @return Post
     */
    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return Collection|MediaObject[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(MediaObject $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
        }

        return $this;
    }

    public function removeImage(MediaObject $image): self
    {
        if ($this->images->contains($image)) {
            $this->images->removeElement($image);
        }

        return $this;
    }

    /**
     * @param string $caption
     * @return Post
     */
    public function setCaption(string $caption): self
    {
        $this->caption = $caption;

        return $this;
    }

    /**
     * @return Collection|PostComment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(PostComment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setPost($this);
        }

        return $this;
    }

    public function removeComment(PostComment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getPost() === $this) {
                $comment->setPost(null);
            }
        }

        return $this;
    }

    /**
     * @return Project
     */
    public function getProject(): Project
    {
        return $this->project;
    }

    /**
     * @param Project $project
     * @return Post
     */
    public function setProject(Project $project): self
    {
        $this->project = $project;

        return $this;
    }

    /**
     * @return int
     */
    public function getCountComments(): int
    {
        return $this->countComments;
    }

    /**
     * @param int $countComments
     * @return Post
     */
    public function setCountComments(int $countComments): self
    {
        $this->countComments = $countComments;

        return $this;
    }

    /**
     * @return Collection|PostUserSmile[]
     */
    public function getPostUserSmiles(): Collection
    {
        return $this->postUserSmiles;
    }

    /**
     * @param PostUserSmile $postUserSmile
     * @return $this
     */
    public function addPostUserSmile(PostUserSmile $postUserSmile): self
    {
        if (!$this->postUserSmiles->contains($postUserSmile)) {
            $this->postUserSmiles[] = $postUserSmile;
            $postUserSmile->setPost($this);
        }

        return $this;
    }

    /**
     * @param PostUserSmile $postUserSmile
     * @return $this
     */
    public function removePostUserSmile(PostUserSmile $postUserSmile): self
    {
        if ($this->postUserSmiles->contains($postUserSmile)) {
            $this->postUserSmiles->removeElement($postUserSmile);
        }

        return $this;
    }
}
