<?php

declare(strict_types=1);

namespace App\Entity\Post;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\User\User;
use App\Repository\Post\PostCommentRepository;
use App\Service\Timestamp\AutoTimestampTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass=PostCommentRepository::class)
 * @ApiResource(
 *     normalizationContext={"groups" = {"post-comment:list"}},
 *     collectionOperations={
 *         "get" = {
 *             "normalization_context" = {"groups" = {"post-comment:list"}}
 *         },
 *         "post" = {
 *             "normalization_context" = {"groups" = {"post-comment:item:post"}},
 *             "denormalization_context" = {"groups" = {"post-comment:post"}},
 *             "security" = "is_granted('ROLE_USER')",
 *         }
 *     },
 *     itemOperations={
 *         "get" = {
 *             "normalization_context" = {"groups" = {"post-comment:item"}},
 *         },
 *         "delete" = {
 *             "security" = "object.getUser() == user",
 *         }
 *     },
 *     attributes={"order" = {"createdAt" : "DESC"}}
 * )
 */
class PostComment
{
    use AutoTimestampTrait;

    /**
     * @var int|null
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"post-comment:item", "post-comment:list", "post-comment:item:post", "post:item", "post:list"})
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     * @Groups({"post-comment:item", "post-comment:list", "post-comment:item:post", "post-comment:post", "post:item", "post:list"})
     */
    private $text;

    /**
     * @var \DateTimeInterface
     * @ORM\Column(type="datetime")
     * @Groups({"post-comment:item", "post-comment:list", "post-comment:item:post", "post:item", "post:list"})
     */
    private $createdAt;

    /**
     * @var \DateTimeInterface
     * @ORM\Column(type="datetime")
     * @Groups({"post-comment:item", "post-comment:list", "post-comment:item:post", "post:item", "post:list"})
     */
    private $updatedAt;

    /**
     * @var Post
     * @ORM\ManyToOne(targetEntity=Post::class, inversedBy="comments")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"post-comment:item", "post-comment:list", "post-comment:item:post", "post-comment:post"})
     */
    private $post;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"post-comment:item", "post-comment:list", "post:item", "post:list"})
     */
    private $user;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     * @return PostComment
     */
    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return Post
     */
    public function getPost(): Post
    {
        return $this->post;
    }

    /**
     * @param Post $post
     * @return PostComment
     */
    public function setPost(Post $post): self
    {
        $this->post = $post;

        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return PostComment
     */
    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
