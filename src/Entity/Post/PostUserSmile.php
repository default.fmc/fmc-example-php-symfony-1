<?php

declare(strict_types=1);

namespace App\Entity\Post;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Entity\User\User;
use App\Repository\Post\PostUserSmileRepository;
use App\Service\Timestamp\DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass=PostUserSmileRepository::class)
 * @ApiResource(
 *     collectionOperations={
 *         "get" = {
 *             "normalization_context" = {"groups" = {"post-user-smile:list"}}
 *         },
 *     },
 *     itemOperations={
 *         "get" = {
 *             "normalization_context" = {"groups" = {"post-user-smile:item"}}
 *         },
 *     },
 *     attributes={"order" = {"createdAt" : "DESC"}}
 * )
 */
class PostUserSmile
{
    /**
     * @var int|null
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"post-user-smile:item", "post-user-smile:list"})
     */
    private $id;

    /**
     * @var Post
     * @ORM\ManyToOne(targetEntity=Post::class, inversedBy="postUserSmiles")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"post-user-smile:item", "post-user-smile:list"})
     * @ApiFilter(SearchFilter::class, properties={"post.id" : "exact"})
     */
    private $post;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="postUserSmiles")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"post-user-smile:item", "post-user-smile:list"})
     * @ApiFilter(SearchFilter::class, properties={"userc.id" : "exact"})
     */
    private $user;

    /**
     * @var \DateTimeInterface
     * @ORM\Column(type="datetime")
     * @Groups({"post-user-smile:item", "post-user-smile:list"})
     */
    private $createdAt;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Post
     */
    public function getPost(): Post
    {
        return $this->post;
    }

    /**
     * @param Post $post
     * @return PostUserSmile
     */
    public function setPost(Post $post): self
    {
        $this->post = $post;

        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return PostUserSmile
     */
    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getCreatedAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTimeInterface $createdAt
     * @return PostUserSmile
     */
    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @ORM\PrePersist
     * @throws \Exception
     */
    public function dateTime(): void
    {
        $this->setCreatedAt(DateTime::getDateTime());
    }
}
