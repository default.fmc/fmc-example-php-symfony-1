<?php

declare(strict_types=1);

namespace App\Entity\Category;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Project\Project;
use App\Repository\Category\CategoryRepository;
use App\Service\Timestamp\AutoTimestampTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=CategoryRepository::class)
 * @ORM\HasLifecycleCallbacks
 * @ApiResource(
 *     attributes={"pagination_enabled" = false},
 *     collectionOperations={
 *         "get" = {
 *             "normalization_context" = {"groups" = {"category:list"}}
 *         }
 *     },
 *     itemOperations={
 *         "get" = {
 *             "normalization_context" = {"groups" = {"category:item"}}
 *         }
 *     }
 * )
 */
class Category
{
    use AutoTimestampTrait;

    /**
     * @var int|null
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"category:item", "category:list", "project:item", "project:list"})
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=100)
     * @Groups({"category:item", "category:list", "project:item", "project:list"})
     */
    private $name;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"category:item", "category:list", "project:item", "project:list"})
     */
    private $description;

    /**
     * @var \DateTimeInterface
     * @ORM\Column(type="datetime")
     * @Groups({"category:item", "category:list", "project:item", "project:list"})
     */
    private $createdAt;

    /**
     * @var \DateTimeInterface
     * @ORM\Column(type="datetime")
     * @Groups({"category:item", "category:list", "project:item", "project:list"})
     */
    private $updatedAt;

    /**
     * @var Collection|Project[]
     * @ORM\OneToMany(targetEntity=Project::class, mappedBy="category")
     * @Groups({"category:item", "category:list"})
     */
    private $projects;

    public function __construct()
    {
        $this->projects = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Category
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return Category
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Project[]
     */
    public function getProjects(): Collection
    {
        return $this->projects;
    }

    /**
     * @param Project $project
     * @return $this
     */
    public function addProject(Project $project): self
    {
        if (!$this->projects->contains($project)) {
            $this->projects[] = $project;
            $project->setCategory($this);
        }

        return $this;
    }

    /**
     * @param Project $project
     * @return $this
     */
    public function removeProject(Project $project): self
    {
        if ($this->projects->contains($project)) {
            $this->projects->removeElement($project);
            // set the owning side to null (unless already changed)
            if ($project->getCategory() === $this) {
                $project->setCategory(null);
            }
        }

        return $this;
    }
}
