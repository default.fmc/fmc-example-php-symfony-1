<?php

declare(strict_types=1);

namespace App\Service\Mailer;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

class ResetPasswordMail
{
    public const SUBJECT = 'Reset password';

    /**
     * @var MailerInterface
     */
    private $mailer;

    /**
     * @param MailerInterface $mailer
     */
    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @param string $email
     * @param string $password
     */
    public function send(string $email, string $password): void
    {
        try {
            $this->mailer->send(
                (new TemplatedEmail())
                    ->subject(self::SUBJECT)
                    ->from(new Address($_ENV['MAILER_FROM'], $_ENV['MAILER_FROM_NAME']))
                    ->to($email)
                    ->htmlTemplate('email/reset-password.html.twig')
                    ->textTemplate('email/reset-password.txt.twig')
                    ->context(['password' => $password])
            );
        } catch (TransportExceptionInterface $e) {
            throw new HttpException(500, 'Email send failed');
        }
    }
}
