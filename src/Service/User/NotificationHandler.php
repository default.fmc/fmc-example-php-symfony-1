<?php

declare(strict_types=1);

namespace App\Service\User;

use App\Entity\MediaObject;
use App\Entity\Post\Post;
use App\Entity\User\User;
use App\Repository\Post\PostUserSmileRepository;
use Liip\ImagineBundle\Service\FilterService;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\Security;

class NotificationHandler
{
    /**
     * @var FilterService
     */
    private $filterService;

    /**
     * @var PostUserSmileRepository
     */
    private $postUserSmileRepository;

    /**
     * @var ParameterBagInterface
     */
    private $bag;

    /**
     * @var Security
     */
    private $security;

    /**
     * @param FilterService $filterService
     * @param PostUserSmileRepository $postUserSmileRepository
     * @param ParameterBagInterface $bag
     * @param Security $security
     */
    public function __construct(
        FilterService $filterService,
        PostUserSmileRepository $postUserSmileRepository,
        ParameterBagInterface $bag,
        Security $security
    ) {
        $this->filterService = $filterService;
        $this->postUserSmileRepository = $postUserSmileRepository;
        $this->bag = $bag;
        $this->security = $security;
    }

    public function handle(User $user): array
    {
        $result = [];

        $result = $this->likedPosts($user, $result);
        $result = $this->followerHistory($user, $result);

        $result = $this->prepareResult($result);

        return [
            'hydra:member' => $result,
            'hydra:totalItems' => \count($result),
        ];
    }

    /**
     * @param User $user
     * @param array $result
     * @return array
     */
    private function likedPosts(User $user, array &$result): array
    {
        foreach ($this->postUserSmileRepository->smilePosts($user) as $postUserSmile) {
            $createdAt = $postUserSmile->getCreatedAt();

            $post = $postUserSmile->getPost();
            $user = $postUserSmile->getUser();

            /** @var User $authUser */
            $authUser = $this->security->getUser();
            $user->setFriend($authUser->getFollowing()->contains($user));

            $this->postMedia($post);

            if ($avatar = $user->getAvatar()) {
                $this->userMedia($avatar);
            }

            $result[$createdAt->getTimestamp()] = [
                'type' => 'like',
                'post' => $post,
                'user' => $user,
                'createdAt' => $createdAt,
            ];
        }

        return $result;
    }

    /**
     * @param User $user
     * @param array $result
     * @return array
     */
    private function followerHistory(User $user, array &$result): array
    {
        foreach ($user->getUserFollowerHistories()->slice(0, 100) as $userFollowerHistory) {
            $createdAt = $userFollowerHistory->getCreatedAt();

            $user = $userFollowerHistory->getFollowing();

            /** @var User $authUser */
            $authUser = $this->security->getUser();

            $user->setFriend($authUser->getFollowing()->contains($user));

            if ($avatar = $user->getAvatar()) {
                $this->userMedia($avatar);
            }

            $result[$createdAt->getTimestamp()] = [
                'type' => 'following',
                'user' => $user,
                'createdAt' => $createdAt,
            ];
        }

        return $result;
    }

    /**
     * @param array $result
     * @return array
     */
    private function prepareResult(array $result): array
    {
        krsort($result);

        return array_values($result);
    }

    /**
     * @param Post $post
     */
    private function postMedia(Post $post): void
    {
        foreach ($post->getImages() as $image) {
            $resourcePath = $this->filterService->getUrlOfFilteredImage(
                $this->bag->get('images_prefix') . '/' . $image->getName(),
                'post_thumbnail'
            );

            $image->setUrl($resourcePath);
        }
    }

    /**
     * @param MediaObject $avatar
     */
    private function userMedia(MediaObject $avatar): void
    {
        $resourcePath = $this->filterService->getUrlOfFilteredImage(
            $this->bag->get('images_prefix') . '/' . $avatar->getName(),
            'user_avatar_thumbnail'
        );

        $avatar->setUrl($resourcePath);
    }
}
