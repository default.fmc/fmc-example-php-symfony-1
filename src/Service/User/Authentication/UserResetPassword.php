<?php

declare(strict_types=1);

namespace App\Service\User\Authentication;

use App\Entity\User\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Component\Validator\Constraints as Assert;

class UserResetPassword
{
    public const PASSWORD_LENGTH = 25;

    /**
     * @var string|null
     * @Assert\Email(
     *     message="The email '{{ value }}' is not a valid email.",
     *     groups={"user:reset-password"}
     * )
     */
    private $email;

    /**
     * @var TokenGeneratorInterface
     */
    private $tokenGenerator;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * @param RequestStack                 $requestStack
     * @param TokenGeneratorInterface      $tokenGenerator
     * @param UserPasswordEncoderInterface $encoder
     * @param EntityManagerInterface       $manager
     */
    public function __construct(RequestStack $requestStack, TokenGeneratorInterface $tokenGenerator, UserPasswordEncoderInterface $encoder, EntityManagerInterface $manager)
    {
        if ($request = $requestStack->getCurrentRequest()) {
            $data = json_decode($request->getContent(), true);

            if (isset($data['email'])) {
                $this->email = $data['email'];
            }
        }

        $this->tokenGenerator = $tokenGenerator;
        $this->encoder = $encoder;
        $this->manager = $manager;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param User $user
     * @return string
     */
    public function reset(User $user): string
    {
        $password = $this->generatePassword();

        $user->setPassword($this->encoder->encodePassword($user, $password));

        $this->manager->persist($user);
        $this->manager->flush();

        return $password;
    }

    /**
     * @return string
     */
    private function generatePassword(): string
    {
        return substr($this->tokenGenerator->generateToken(), 0, self::PASSWORD_LENGTH);
    }
}
