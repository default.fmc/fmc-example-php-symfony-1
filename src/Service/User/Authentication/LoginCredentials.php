<?php

declare(strict_types=1);

namespace App\Service\User\Authentication;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Validator\Constraints as Assert;

class LoginCredentials
{
    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    /**
     * @var string
     * @Assert\Length(
     *     min=8,
     *     max=50,
     *     allowEmptyString=false
     * )
     */
    private $password;

    /**
     * @param RequestStack       $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        if ($request = $requestStack->getCurrentRequest()) {
            $data = json_decode($request->getContent(), true);

            if (isset($data['email'])) {
                $this->email = $data['email'];
            }

            if (isset($data['password'])) {
                $this->password = $data['password'];
            }
        }
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }
}
