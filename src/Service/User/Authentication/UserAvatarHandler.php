<?php

declare(strict_types=1);

namespace App\Service\User\Authentication;

use App\Entity\User\User;
use Liip\ImagineBundle\Service\FilterService;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class UserAvatarHandler
{
    /**
     * @var FilterService
     */
    private $filter;

    /**
     * @var ParameterBagInterface
     */
    private $bag;

    /**
     * @param FilterService         $filter
     * @param ParameterBagInterface $bag
     */
    public function __construct(FilterService $filter, ParameterBagInterface $bag)
    {
        $this->filter = $filter;
        $this->bag = $bag;
    }

    /**
     * @param User $user
     */
    public function handle(User $user): void
    {
        if ($user->getAvatar() !== null) {
            $resourcePath = $this->filter->getUrlOfFilteredImage(
                $this->bag->get('images_prefix') .
                '/' .
                $user->getAvatar()->getName(),
                'user_avatar_thumbnail'
            );

            $user->getAvatar()->setUrl($resourcePath);
        }
    }
}
