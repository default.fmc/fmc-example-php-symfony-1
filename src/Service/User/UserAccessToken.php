<?php

declare(strict_types=1);

namespace App\Service\User;

use App\Entity\User\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

class UserAccessToken
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * @var TokenGeneratorInterface
     */
    private $generator;

    /**
     * @param EntityManagerInterface  $manager
     * @param TokenGeneratorInterface $generator
     */
    public function __construct(EntityManagerInterface $manager, TokenGeneratorInterface $generator)
    {
        $this->manager = $manager;
        $this->generator = $generator;
    }

    /**
     * @return string
     */
    public function generate(): string
    {
        return $this->generator->generateToken();
    }

    /**
     * @param User $user
     */
    public function update(User $user): void
    {
        $user->setAccessToken($this->generate());
        $this->manager->persist($user);
        $this->manager->flush();
    }
}
