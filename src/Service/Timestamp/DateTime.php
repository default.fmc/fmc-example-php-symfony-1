<?php

declare(strict_types=1);

namespace App\Service\Timestamp;

class DateTime
{
    /**
     * @param string $time
     * @throws \Exception
     * @return \DateTime
     */
    public static function getDateTime(string $time = 'now'): \DateTime
    {
        return (new \DateTime($time))->setTimezone(new \DateTimeZone('GMT-0'));
    }
}
