# fmc-example-php-symfony

*A social network like Instagram, where projects are created to collect voluntary donations.*

___

#### Powered by Symfony framework and Api platform

___

Do this to run the application

```bash
$ git clone https://gitlab.com/default.fmc/fmc-example-php-symfony-1.git
```

```bash
$ cd fmc-example-php-symfony-1 && composer install
```

```bash
$ composer dump-env dev
```

Configure database in `.env.local.php`

```bash
$ php bin/console doctrine:database:create 
```

```bash
$ php bin/console make:migration && php bin/console doctrine:migrations:migrate
```

```bash
$ php bin/console doctrine:fixtures:load
```
___
**Start PHP server:**

```bash
$ php -S 0.0.0.0:8000 /public -t
```

or if you have [Symfony CLI](https://symfony.com/download)

```bash
$ symfony serve
```
___
**And let's go try:** http://localhost:8000/api